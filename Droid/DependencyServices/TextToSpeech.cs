﻿using System;

using TestDependencyService;
using Xamarin.Forms;
using TestDependencyService.Droid;

using Android.Speech.Tts;
using Android.Runtime;


[assembly: Dependency(typeof(TextToSpeechDroid))]
namespace TestDependencyService.Droid
{
    public class TextToSpeechDroid : Java.Lang.Object, ITextToSpeech, TextToSpeech.IOnInitListener
    {
        public TextToSpeechDroid()
        {
        }

        TextToSpeech speaker;
        string toSpeak;

        public void OnInit([GeneratedEnum] OperationResult status)
        {
            if(status.Equals(OperationResult.Success))
            {
                speaker.Speak(toSpeak, QueueMode.Flush, null, null);
            }
        }

        public void Speak(string text)
        {
            toSpeak = text;
            if(speaker == null)
            {
                speaker = new TextToSpeech(Forms.Context, this);
            }
            else
            {
                speaker.Speak(toSpeak, QueueMode.Flush, null,null);
            }
        }
    }
}
