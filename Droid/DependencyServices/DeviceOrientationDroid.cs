﻿using System;
using Xamarin.Forms;

using TestDependencyService;
using TestDependencyService.Droid;

using Android.Hardware;
using Android.Views;
using Android.Content;
using Android.Runtime;


[assembly: Xamarin.Forms.Dependency(typeof(DeviceOrientationDroid))]
namespace TestDependencyService.Droid
{
    public class DeviceOrientationDroid : IDeviceOrientation
    {
        public DeviceOrientationDroid()
        {
        }

        public static void Init() { }

        public DeviceOrientation GetOrientation()
        {
            IWindowManager windowManager = Android.App.Application.Context.GetSystemService(Context.WindowService).JavaCast<IWindowManager>();

            var rotation = windowManager.DefaultDisplay.Rotation;
            bool isLandscape = rotation == SurfaceOrientation.Rotation90 || rotation == SurfaceOrientation.Rotation270;
                                                      
            return isLandscape ? DeviceOrientation.Landscape : DeviceOrientation.Portrait;
        }
    }
}
