﻿using System;
using System.Threading;
using System.Threading.Tasks;

using Android.App;
using Android.Hardware.Fingerprints;
using Android.OS;
using Android.Support.V4.Hardware.Fingerprint;
using Android.Util;
using Android.Views;
using Android.Widget;
using CancellationSignal = Android.Support.V4.OS.CancellationSignal;
using Res = Android.Resource;

using Java.Lang;
using Javax.Crypto;
using Android;

using TestDependencyService;
using TestDependencyService.Droid;

namespace TestDependencyService.Droid
{
    /// <summary>
    ///     This DialogFragment is displayed when the app is scanning for fingerprints.
    /// </summary>
    /// <remarks>
    ///     This DialogFragment doesn't perform any checks to see if the device
    ///     is actually eligible for fingerprint authentication. All of those checks are performed by the
    ///     Activity.
    /// </remarks>
    public class FingerprintManagerApiDialogFragment : Android.App.DialogFragment
    {
        static readonly string _TAG = "X:" + typeof(FingerprintManagerApiDialogFragment).Name;
        FingerprintAuthenticatedResult _result;

        Button _cancelButton;
        CancellationSignal _cancellationSignal;
        CancellationToken _cancellationToken;

        FingerprintManagerCompat _fingerprintManager;
        CryptoObjectHelper _CryptObjectHelper { get; set; }

        bool _IsScanningForFingerprints
        {
            // ReSharper disable once ConvertPropertyToExpressionBody
            get 
            {
                return _cancellationSignal != null;
            }
        }

        public bool ScanForFingerprintsInOnResume { get; set; } = true;
        public bool UserCancelledScan { get; set; }
        public FingerprintAuth Called;

        public static FingerprintManagerApiDialogFragment NewInstance(FingerprintManagerCompat fingerprintManager)
        {
            FingerprintManagerApiDialogFragment frag = new FingerprintManagerApiDialogFragment
            {
                _fingerprintManager = fingerprintManager
            };
            return frag;
        }

        public static FingerprintManagerApiDialogFragment NewInstance(FingerprintAuthenticatedResult result, FingerprintManagerCompat fingerprintManager, CancellationToken cancellationToken, FingerprintAuth called)
        {
            FingerprintManagerApiDialogFragment frag = new FingerprintManagerApiDialogFragment
            {
                _fingerprintManager = fingerprintManager,
                _result = result,
                _cancellationToken = cancellationToken,
                Called = called
            };
            return frag;
        }

        public void Init(bool startScanning = true)
        {
            ScanForFingerprintsInOnResume = startScanning;
        }

        public override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            RetainInstance = true;
            _CryptObjectHelper = new CryptoObjectHelper();
            SetStyle(DialogFragmentStyle.Normal, Res.Style.ThemeMaterialLightDialog);
        }

        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            Dialog.SetTitle(Resource.String.cancel);

            View v = inflater.Inflate(Resource.Layout.dialog_scanning_for_fingerprint, container, false);

            _cancelButton = v.FindViewById<Button>(Resource.Id.cancel_button);
            _cancelButton.Click += (sender, args) =>
            {
                UserCancelledScan = true;
                StopListeningForFingerprints();
            };

            return v;
        }

        public override void OnResume()
        {
            base.OnResume();
            if (!ScanForFingerprintsInOnResume)
            {
                return;
            }

            UserCancelledScan = false;

            _cancellationSignal = new CancellationSignal();
            _cancellationToken.Register(() => _cancellationSignal.Cancel());
            _fingerprintManager.Authenticate(_CryptObjectHelper.BuildCryptoObject(),
                                             (int)FingerprintAuthenticationFlags.None,
                                             _cancellationSignal,
                                             new FingerpritAuthCallback(_result, this),
                                             null);
        }

        public override void OnPause()
        {
            base.OnPause();
            if (_IsScanningForFingerprints)
            {
                StopListeningForFingerprints(true);
            }
        }

        void StopListeningForFingerprints(bool butStartListeningAgainInOnResume = false)
        {
            if (_cancellationSignal != null)
            {
                if (!_cancellationSignal.IsCanceled)
                {
                    _cancellationSignal.Cancel();
                    _cancellationSignal = null;
                    Log.Debug(_TAG, "StopListeningForFingerprints: _cancellationSignal.Cancel();");
                }
            }
            ScanForFingerprintsInOnResume = butStartListeningAgainInOnResume;
        }

        public override void OnDestroyView()
        {
            Called.SetResult(_result);

            // see https://code.google.com/p/android/issues/detail?id=17423
            if (Dialog != null && RetainInstance)
            {
                Dialog.SetDismissMessage(null);
            }
            base.OnDestroyView();
        }
    }
}
