﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Android.Hardware.Fingerprints;
using Android.Support.V4.Hardware.Fingerprint;
using Android.Util;
using Java.Lang;
using Javax.Crypto;
using TestDependencyService;
using TestDependencyService.Droid;


namespace TestDependencyService.Droid
{
    public class FingerpritAuthCallback : FingerprintManagerCompat.AuthenticationCallback
    {
        FingerprintAuthenticatedResult _result;

        readonly FingerprintManagerApiDialogFragment _fragment;
        // Can be any byte array, keep unique to application.
        static readonly byte[] _SECRET_BYTES = { 1, 2, 3, 4, 5, 6, 7, 8, 9 };
        // The TAG can be any string, this one is for demonstration.
        static readonly string _TAG = "X:" + typeof(FingerpritAuthCallback).Name;

        public FingerpritAuthCallback(FingerprintManagerApiDialogFragment frag)
        {
            _result = null;
            _fragment = frag;
        }

        public FingerpritAuthCallback(FingerprintAuthenticatedResult authRes, FingerprintManagerApiDialogFragment frag)
        {
            _result = authRes;
            _fragment = frag;
        }

        public override void OnAuthenticationSucceeded(FingerprintManagerCompat.AuthenticationResult result)
        {
            Log.Debug(_TAG, "OnAuthenticationSucceeded");
            if (result.CryptoObject.Cipher != null)
            {
                try
                {
                    // Calling DoFinal on the Cipher ensures that the encryption worked.
                    byte[] doFinalResult = result.CryptoObject.Cipher.DoFinal(_SECRET_BYTES);
                    Log.Debug(_TAG, "Fingerprint authentication succeeded, doFinal results: {0}",
                                  Convert.ToBase64String(doFinalResult));
                    // No errors occurred, trust the results.
                    ReportSuccess();
                }
                catch (BadPaddingException bpe)
                {
                    // Can't really trust the results.
                    Log.Error(_TAG, "Failed to encrypt the data with the generated key." + bpe);
                    ReportAuthenticationFailed();
                }
                catch (IllegalBlockSizeException ibse)
                {
                    // Can't really trust the results.
                    Log.Error(_TAG, "Failed to encrypt the data with the generated key." + ibse);
                    ReportAuthenticationFailed();
                }
            }
            else
            {
                // No cipher used, assume that everything went well and trust the results.
                Log.Debug(_TAG, "Fingerprint authentication succeeded.");
                ReportSuccess();
            }
        }

        public override void OnAuthenticationError(int errMsgId, ICharSequence errString)
        {
            // Report the error to the user. Note that if the user canceled the scan,
            // this method will be called and the errMsgId will be FingerprintState.ErrorCanceled.

            // There are some situations where we don't care about the error. For example, 
            // if the user cancelled the scan, this will raise errorID #5. We don't want to
            // report that, we'll just ignore it as that event is a part of the workflow.
            bool reportError = (errMsgId == (int)FingerprintState.ErrorCanceled) &&
                        !_fragment.ScanForFingerprintsInOnResume;

            string debugMsg = string.Format("OnAuthenticationError: {0}:`{1}`.", errMsgId, errString);

            if (_fragment.UserCancelledScan)
            {
                string msg = _fragment.Resources.GetString(Resource.String.scan_cancelled_by_user);
                ReportScanFailure(-1, msg);
            }
            else if (reportError)
            {
                ReportScanFailure(errMsgId, errString.ToString());
                debugMsg += " Reporting the error.";
            }
            else
            {
                debugMsg += " Ignoring the error.";
                // ReportAuthenticationFailed();
            }
            Log.Debug(_TAG, debugMsg);
        }

        public override void OnAuthenticationFailed()
        {
            // Tell the user that the fingerprint was not recognized.
            Log.Info(_TAG, "Authentication failed.");
            ReportAuthenticationFailed();
        }

        public override void OnAuthenticationHelp(int helpMsgId, ICharSequence helpString)
        {
            // Notify the user that the scan failed and display the provided hint.
            Log.Debug(_TAG, "OnAuthenticationHelp: {0}:`{1}`", helpString, helpMsgId);
            ReportScanFailure(helpMsgId, helpString.ToString());
        }

        void ReportSuccess()
        {
            SetResult(true);
        }

        void ReportScanFailure(int errMsgId, string errorMessage)
        {
            SetResult(false);
        }

        void ReportAuthenticationFailed()
        {
            SetResult(false);
        }

        void SetResult(bool auth)
        {
            _result.isAutheticated = auth;
            _fragment.Dismiss();
        }
    }
}
