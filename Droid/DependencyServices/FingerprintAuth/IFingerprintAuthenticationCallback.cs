﻿using System;
using System.Threading;
using System.Threading.Tasks;
using TestDependencyService;

namespace TestDependencyService.Droid
{
    public interface IFingerprintAuthenticationCallback : IDisposable
    {
        Task<FingerprintAuthenticatedResult> GetTask();
    }
}
