﻿using System;
using System.Threading;
using System.Threading.Tasks;

using Android.OS;
using Android.App;
using Android.Content;
using Android.Runtime;
using Android.Content.PM;
using Android.Util;
using Android.Views;
using Android.Widget;

using Android.Security.Keystore;
using Android.Hardware.Fingerprints;
using Android.Support.V4.Content;
using Android.Support.V4.Hardware.Fingerprint;
using Res = Android.Resource;

using TestDependencyService;
using TestDependencyService.Droid;
using Android;
using Android.Support.Design.Widget;
using static Android.Hardware.Fingerprints.FingerprintManager;

[assembly: Xamarin.Forms.Dependency(typeof(FingerprintAuth))]
namespace TestDependencyService.Droid
{
    public class FingerprintAuth : IFingerprintAuth
    {
        static readonly string _DIALOG_FRAGMENT_TAG = "fingerprint_auth_fragment";

        FingerprintManagerApiDialogFragment _dialogFrag;
        FingerprintManagerCompat _fingerprintManager;
        Context _context;
        MainActivity _activity;

        FingerprintAuthenticatedResult _result;
        TaskCompletionSource<FingerprintAuthenticatedResult> _taskCompletionSource;

        public FingerprintAuth()
        {
            _context = Xamarin.Forms.Forms.Context;
            _activity = Xamarin.Forms.Forms.Context as MainActivity;
            _fingerprintManager = FingerprintManagerCompat.From(_context);
        }

        public async Task<bool> CanAuthenticate(bool devicePassword = false)
        {
            bool res = false;

            if (_context != null)
            {
                Permission permissionResult = ContextCompat.CheckSelfPermission(_context, Manifest.Permission.UseFingerprint);
                if (permissionResult == Permission.Granted)
                {
                    _fingerprintManager = FingerprintManagerCompat.From(_context);
                    KeyguardManager keyguardManager = (KeyguardManager)_context.GetSystemService(Context.KeyguardService);
                    res = _fingerprintManager.IsHardwareDetected 
                                            && _fingerprintManager.HasEnrolledFingerprints 
                                            && keyguardManager.IsKeyguardSecure;
                }
            }
            return res;
        }

        public async Task<FingerprintAuthenticatedResult> GetAuthenticateAsync(string reason, bool devicePassword = false, CancellationToken cancellationToken = default(CancellationToken))
        {
            _taskCompletionSource = new TaskCompletionSource<FingerprintAuthenticatedResult>();

            Authenticate(cancellationToken);

            _result = await _taskCompletionSource.Task;

            return _result;
        }

        public void SetResult(FingerprintAuthenticatedResult result)
        {
            _taskCompletionSource.TrySetResult(result);
        }

        void Authenticate(CancellationToken cancellationToken)
        {
            _result = new FingerprintAuthenticatedResult();

            _fingerprintManager = FingerprintManagerCompat.From(_context);
            _dialogFrag = FingerprintManagerApiDialogFragment.NewInstance(_result, _fingerprintManager, cancellationToken, this);
            _dialogFrag.Init();
            _dialogFrag.Show(_activity.FragmentManager, _DIALOG_FRAGMENT_TAG);
        }
    }
}
