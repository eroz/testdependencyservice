﻿using System.Threading;
using System.Threading.Tasks;


namespace TestDependencyService
{
    public interface IFingerprintAuth
    {
        Task<bool> CanAuthenticate(bool devicePassword = false);

        Task<FingerprintAuthenticatedResult> GetAuthenticateAsync (string reason, bool devicePassword = false, CancellationToken cancellationToken = default(CancellationToken));
    }

    public class FingerprintAuthenticatedResult
    {
        public FingerprintAuthenticatedResult()
        {
            isAutheticated = false;
            ErrorCode = -1;
            ErrorMessage = "";
        }
        public bool isAutheticated { get; set;  } 
        public string ErrorMessage { get; set; }
        public int ErrorCode { get; set; }

    }
}
