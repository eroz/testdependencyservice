﻿using System;
using System.IO;
using System.Threading.Tasks;

namespace TestDependencyService
{
    public interface IPicturePicker
    {
        Task<Stream> GetImageStreamAsync();
    }
}
