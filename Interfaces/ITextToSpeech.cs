﻿using System;

namespace TestDependencyService
{
    public interface ITextToSpeech
    {
        void Speak(string text);
    }
}
