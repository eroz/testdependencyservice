﻿using System;

namespace TestDependencyService
{
    public enum DeviceOrientation
    {
        Undefined,
        Landscape,
        Portrait
    }
    public interface IDeviceOrientation
    {
        DeviceOrientation GetOrientation();
    }
}
