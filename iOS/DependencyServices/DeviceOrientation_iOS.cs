﻿using System;

using Xamarin.Forms;
using TestDependencyService;
using TestDependencyService.iOS;

using UIKit;
using Foundation;

[assembly: Dependency(typeof(DeviceOrientation_iOS))]
namespace TestDependencyService.iOS
{
    public class DeviceOrientation_iOS : IDeviceOrientation
    {
        public DeviceOrientation_iOS()
        {
        }


        public DeviceOrientation GetOrientation()
        {
            var currentOrientation = UIApplication.SharedApplication.StatusBarOrientation;
            bool isPortrait = currentOrientation == UIInterfaceOrientation.Portrait ||
              currentOrientation == UIInterfaceOrientation.PortraitUpsideDown;

            return isPortrait ? DeviceOrientation.Portrait : DeviceOrientation.Landscape;
        }
    }
}
