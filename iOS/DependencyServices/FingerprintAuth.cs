﻿using System;
using System.Threading.Tasks;

using TestDependencyService;
using TestDependencyService.iOS;

using UIKit;
using Foundation;
using LocalAuthentication;
using Security;
using System.Threading;
using ObjCRuntime;

[assembly: Xamarin.Forms.Dependency(typeof(FingerprintAuth))]
namespace TestDependencyService.iOS
{
    public class FingerprintAuth : IFingerprintAuth
    {
        private bool _isAvailable;
        private LAContext context;

        public FingerprintAuth()
        {
            _isAvailable = false;
            CreateLAContext();
        }

        public async Task<FingerprintAuthenticatedResult> GetAuthenticateAsync(string reason, bool devicePassword = false, CancellationToken cancellationToken = default(CancellationToken))
        {
            var res = new FingerprintAuthenticatedResult();

            try
            {
                // var context = new LAContext();
                var localizedReason = new NSString(reason);
                Tuple<bool, NSError> auth;

                using(cancellationToken.Register(CancelAuthentication))
                {
                    if (GetAvailability(devicePassword))
                    {
                        var policy = devicePassword ? LAPolicy.DeviceOwnerAuthentication : LAPolicy.DeviceOwnerAuthenticationWithBiometrics;
                        auth = await context.EvaluatePolicyAsync(policy, reason);
                        res.isAutheticated = auth.Item1;

                        if (!res.isAutheticated)
                        {
                            res.ErrorMessage = auth.Item2.Description;
                            res.ErrorCode = (int)Convert.ToInt32(auth.Item2.Code);
                        }
                    }
                    else
                    {
                        //Error no available

                        res.isAutheticated = false;
                        res.ErrorMessage = "No Touch ID Available";
                        res.ErrorCode = 1;
                    }
                }
            }
            catch(Exception ex)
            {
                System.Diagnostics.Debug.WriteLine(ex.Message);

                res.isAutheticated = false;
                res.ErrorMessage = ex.Message;
                res.ErrorCode = 0;
            }

            return res;
        }
        
        public async Task<bool> CanAuthenticate(bool devicePassword = false)
        {

            return GetAvailability(devicePassword);
        }

        private bool GetAvailability(bool useDevicePassword)
        {
            NSError error;

            if(context == null)
            {
                _isAvailable = false;
            }
            else
            {
                var policy = useDevicePassword ? LAPolicy.DeviceOwnerAuthentication : LAPolicy.DeviceOwnerAuthenticationWithBiometrics;
                _isAvailable = context.CanEvaluatePolicy(policy, out error);
                if(_isAvailable)
                {
                    // chaeck the error
                }
            }
            return _isAvailable;
        }

        private void CancelAuthentication()
        {
            CreateNewContext();
        }

        private void CreateNewContext()
        {
            if (context != null)
            {
                if (context.RespondsToSelector(new Selector("invalidate")))
                {
                    context.Invalidate();
                }
                context.Dispose();
            }

            CreateLAContext();
        }

        private void CreateLAContext()
        {
            var processInfo = new NSProcessInfo();

            if(UIDevice.CurrentDevice.CheckSystemVersion(8,0))
            {
                // Check LAContext is not available on iOS7 and below, so check LAContext after checking iOS version.
                if (Class.GetHandle(typeof(LAContext)) == IntPtr.Zero)
                    return;

                context = new LAContext();
            }
        }
    }
}
