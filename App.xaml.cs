﻿using Xamarin.Forms;

namespace TestDependencyService
{
    public partial class App : Application
    {
        public App()
        {
            InitializeComponent();

            // MainPage = new TestDependencyServicePage();

            #region code

            var stack = new StackLayout();

            MainPage = new TestDependencyServicePage
            {
                Content = stack
            };

            // Fingerprint authenticate
            var fingerprint = new Button
            {
                Text = "Authenticate FigerPrint",
                VerticalOptions = LayoutOptions.CenterAndExpand,
                HorizontalOptions = LayoutOptions.CenterAndExpand,
            };
            fingerprint.Clicked += async (sender, e) => {

                var fingerprintAuth = DependencyService.Get<IFingerprintAuth>();
                var canAuth = await fingerprintAuth.CanAuthenticate(true);
                if (canAuth)
                {
                    var auth = await fingerprintAuth.GetAuthenticateAsync("to login", true);
                    if (auth.isAutheticated)
                    {
                        fingerprint.Text = "Authenticate FigerPrint - Ok";
                    }
                    else
                    {
                        fingerprint.Text = "Authenticate FigerPrint - No-Auth";
                    }
                }
                else
                {
                    fingerprint.Text = "Authenticate FigerPrint - Error, not available";
                }
            };

            stack.Children.Add(fingerprint);


            // Text-to-Speech
            var speak = new Button
            {
                Text = "Hello, Forms !",
                VerticalOptions = LayoutOptions.CenterAndExpand,
                HorizontalOptions = LayoutOptions.CenterAndExpand,
            };
            speak.Clicked += (sender, e) => {
                DependencyService.Get<ITextToSpeech>().Speak("Hello from Xamarin Forms!");
            };
            stack.Children.Add(speak);


            // Device Orientation
            var orient = new Button
            {
                Text = "Get Orientation",
                VerticalOptions = LayoutOptions.CenterAndExpand,
                HorizontalOptions = LayoutOptions.CenterAndExpand,
            };
            orient.Clicked += (sender, e) => {
                var orientation = DependencyService.Get<IDeviceOrientation>().GetOrientation();
                switch (orientation)
                {
                    case DeviceOrientation.Undefined:
                        orient.Text = "Undefined";
                        break;
                    case DeviceOrientation.Portrait:
                        orient.Text = "Portrait";
                        break;
                    case DeviceOrientation.Landscape:
                        orient.Text = "Landscape";
                        break;
                }
            };
            stack.Children.Add(orient);


            // Battery Information
            var batteryInfo = new Button
            {
                Text = "Click for battery info",
                VerticalOptions = LayoutOptions.CenterAndExpand,
                HorizontalOptions = LayoutOptions.CenterAndExpand,
            };
            batteryInfo.Clicked += (sender, e) => {
                var bat = DependencyService.Get<IBattery>();

                switch (bat.PowerSource)
                {
                    case PowerSource.Battery:
                        batteryInfo.Text = "Battery - ";
                        break;
                    case PowerSource.Ac:
                        batteryInfo.Text = "AC - ";
                        break;
                    case PowerSource.Usb:
                        batteryInfo.Text = "USB - ";
                        break;
                    case PowerSource.Wireless:
                        batteryInfo.Text = "Wireless - ";
                        break;
                    case PowerSource.Other:
                    default:
                        batteryInfo.Text = "Other - ";
                        break;
                }
                switch (bat.Status)
                {
                    case BatteryStatus.Charging:
                        batteryInfo.Text += "Charging";
                        break;
                    case BatteryStatus.Discharging:
                        batteryInfo.Text += "Discharging";
                        break;
                    case BatteryStatus.NotCharging:
                        batteryInfo.Text += "Not Charging";
                        break;
                    case BatteryStatus.Full:
                        batteryInfo.Text += "Full";
                        break;
                    case BatteryStatus.Unknown:
                    default:
                        batteryInfo.Text += "Unknown";
                        break;
                }
            };
            stack.Children.Add(batteryInfo);


            // Picture Picker
            Button pickPictureButton = new Button
            {
                Text = "Pick Photo",
                VerticalOptions = LayoutOptions.CenterAndExpand,
                HorizontalOptions = LayoutOptions.CenterAndExpand
            };
            stack.Children.Add(pickPictureButton);
            pickPictureButton.Clicked += async (sender, e) => {
                pickPictureButton.IsEnabled = false;

                var stream = await DependencyService.Get<IPicturePicker>().GetImageStreamAsync();

                if (stream != null)
                {
                    Image image = new Image
                    {
                        Source = ImageSource.FromStream(() => stream),
                        BackgroundColor = Color.Gray
                    };

                    TapGestureRecognizer recognizer = new TapGestureRecognizer();
                    recognizer.Tapped += (sender2, args) =>
                    {
                        (MainPage as ContentPage).Content = stack;
                        pickPictureButton.IsEnabled = true;
                    };
                    image.GestureRecognizers.Add(recognizer);

                    (MainPage as ContentPage).Content = image;


                    pickPictureButton.Text = "Pick Photo";
                }
                else
                {
                    pickPictureButton.IsEnabled = true;
                    pickPictureButton.Text = "Error, try Pick Photo again";
                }
            };

            #endregion
        }

        protected override void OnStart()
        {
            // Handle when your app starts
        }

        protected override void OnSleep()
        {
            // Handle when your app sleeps
        }

        protected override void OnResume()
        {
            // Handle when your app resumes
        }
    }
}
